package org.sber;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.*;

public class MatrixService {

    public int sum(int[][] matrix, int nthreads) throws ExecutionException, InterruptedException {
        int sum = 0;
        Queue<Future<Integer>> futures = new LinkedList<>();
        ExecutorService executorService = Executors.newFixedThreadPool(nthreads);
        for (int i = 0; i < matrix.length; i++) {
            ColumnSummator columnSummator = new ColumnSummator(matrix[i]);
            futures.add(executorService.submit(columnSummator));
        }
        executorService.shutdown();
        while(!futures.isEmpty()) {
            sum += futures.poll().get();
        }
        return sum;
    }

    class ColumnSummator implements Callable<Integer> {

        int[] array;

        public ColumnSummator(int[] array) {
            this.array = array;
        }

        @Override
        public Integer call() throws Exception {
            return Arrays.stream(array).sum();
        }
    }
}